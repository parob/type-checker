# -*- coding: utf-8 -*-
#
# Configuration file for the Sphinx documentation builder.
#
# This file does only contain a selection of the most common options. For a
# full list see the documentation:
# http://www.sphinx-doc.org/en/stable/config

project = 'Type Checker'
copyright = '2019, Robert Parker'
author = 'Robert Parker'

version = '1.0'
release = '1.0.1'

extensions = [
    'sphinx.ext.autodoc',
    'sphinxcontrib.fulltoc'
]

templates_path = ['_templates']
source_suffix = '.rst'
master_doc = 'index'
language = None
exclude_patterns = ['_build']
pygments_style = 'sphinx'


# -- Options for HTML output -------------------------------------------------

html_theme = 'alabaster'
html_static_path = ['_static']
html_favicon = '_static/favicon.ico'
html_sidebars = {'**': ['sidebarlogo.html', 'sidebarintro.html', 'globaltoc.html', 'relations.html', 'sourcelink.html', 'searchbox.html'], }

